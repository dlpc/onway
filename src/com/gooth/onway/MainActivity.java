package com.gooth.onway;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;


public class MainActivity extends Activity implements View.OnClickListener
{
	public LocationClient locationClient=null;
	public LocationUpdater locationUploader=null;

	class btn_save_listener implements View.OnClickListener
	{
		@Override
		public void onClick(View view)
		{
			SharedPreferences pref = getPreferences(0);
			SharedPreferences.Editor prefEditor = pref.edit();

			EditText edtServerUri = (EditText)findViewById(R.id.edt_server_uri);
			EditText edtRequestTimeout = (EditText)findViewById(R.id.edt_request_timeout);

			//设置
			prefEditor.putString("server_uri", edtServerUri.getText().toString());
			prefEditor.putString("request_timeout", edtRequestTimeout.getText().toString());
			prefEditor.commit();
		}
	}

	@Override
	public void onClick(View view)
	{
		EditText editTimeout = (EditText)findViewById(R.id.edt_request_timeout);
		int timeout = Integer.valueOf(editTimeout.getText().toString());
		EditText editServer = (EditText)findViewById(R.id.edt_server_uri);
		String server_uri = editServer.getText().toString();
		
		TextView txtLog = (TextView) findViewById(R.id.txt_log);
		
		if (server_uri==null || server_uri==""){
			txtLog.append("Invalid parameters!");
			return;
		}

		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		//option.setAddrType("all");
		option.setCoorType("bd09ll");
		option.setPriority(LocationClientOption.NetWorkFirst);
		option.setProdName("TestDemo");
		option.setScanSpan(timeout*1000);
		option.disableCache(true);
		option.setPoiNumber(5);
		option.setPoiDistance(1000);
		option.setPoiExtraInfo(true);

		if (locationClient==null) {
			locationClient = new LocationClient(getApplicationContext());
			locationUploader = new LocationUpdater();
		}
		locationUploader.setServerUri(server_uri);
		locationClient.setLocOption(option);
		//locationClient.registerLocationListener(locationUploader);

		switch (view.getId()) {
			case R.id.btn_start:
				if (locationClient.isStarted()) {
					Log.d("OnWay","Restart service");
					locationClient.unRegisterLocationListener(locationUploader);
					locationClient.stop();
				} else {
					Log.d("OnWay","Start service");
				}
				locationClient.registerLocationListener(locationUploader);
				locationClient.start();
				locationClient.requestLocation();
				break;
			case R.id.btn_stop:
				Log.d("OnWay","Stop service");
				locationClient.unRegisterLocationListener(locationUploader);
				locationClient.stop();
				break;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		loadPreference();

		findViewById(R.id.btn_start).setOnClickListener(this);
		findViewById(R.id.btn_stop).setOnClickListener(this);
		findViewById(R.id.btn_save).setOnClickListener(new btn_save_listener());
	}
	
	protected void loadPreference()
	{
		//获取默认值
		String server_uri_default = getResources().getString(R.string.server_uri_default);
		String request_timeout_default = getResources().getString(R.string.request_timeout_default);

		//获取配置值
		SharedPreferences pref = getPreferences(0);
		String server_uri = pref.getString("server_uri", server_uri_default);
		String request_timeout = pref.getString("request_timeout", request_timeout_default);
		
		//更新界面
		EditText edtServerUri = (EditText)findViewById(R.id.edt_server_uri);
		EditText edtRequestTimeout = (EditText)findViewById(R.id.edt_request_timeout);

		edtServerUri.setText(server_uri);
		edtRequestTimeout.setText(request_timeout);
	}
}
